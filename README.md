### Installation

- first clone the project

```
git clone https://gitlab.com/nima.2004hkh/path_project_symfony4.git
```

- edit `.env` and set your database credentials

- install composer and install dependencies

```
composer install
```

- config jwt secret keys

```
$ mkdir -p config/jwt
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
$ openssl rsa -in config/jwt/private.pem -out config/jwt/private2.pem
$ mv config/jwt/private.pem config/jwt/private.pem-back
$ mv config/jwt/private2.pem config/jwt/private.pem

```

- migrating Databse

```
php bin/console make:migration
php bin/console doctrine:migrations:migrate

```

### everything is ready now.

install `symfony` cli, then just run :

```
symfony serve
```

##### installing symfony cli

[installing symfony cli](https://symfony.com/download)

### Postman

upload `postman.json` into your postman as an collection.
