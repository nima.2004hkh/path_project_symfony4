<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthController extends AbstractController
{
    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     * @Route("/register", name="register", methods="POST")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder,ValidatorInterface $validator)
    {
        $em = $this->getDoctrine()->getManager();
        $serializedUser;

        $username = $request->request->get('username');
        $password = $request->request->get('password');
        try{

            $user = new User($username);
            $user->setPassword($encoder->encodePassword($user, $password));
            $errors = $validator->validate($user);

            //if has error, return it

            if(count($errors)>0){
                $serializedUser = new Response([],(string) $errors,500);
                return $serializedUser->toJson();
            }

            $em->persist($user);
            $em->flush();

            $serializedUser = new Response(["username"=>$user->getUsername()],"user registered successfully",200);
        }
        catch(DBALException $e){
            $errorMessage = $e->getMessage();

            $serializedUser = new Response([],$errorMessage,500);
        }
        catch(\Exception $e){
            $errorMessage = $e->getMessage();

            $serializedUser = new Response([],$errorMessage,500);
        }

        return $serializedUser->toJson();
    }
}
