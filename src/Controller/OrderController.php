<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Products;
use App\Repository\OrdersRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Orders;
use App\Utils\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderController extends AbstractController
{
    public function __constructor(){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
    }

    /**
     * List all orders
     * @Route("/api/orders", name="order", methods="GET")
     */
    public function index(OrdersRepository $OrdersRepository)
    {
        $user = $this->getUser();

        $orders =  $OrdersRepository->getAllOrders($user);

        $serialized= new Response($orders, "" ,200);
        return $serialized->toJson();
    }

    /**
     * insert into Orders
     * @Route("/api/orders/new", methods="POST")
     */
    public function insert(Request $request,ValidatorInterface $validator)
    {

        $productId = $request->request->get('product_id');

        //fetch product
        $product =  $this->getDoctrine()
                         ->getRepository(Products::class)
                         ->find($productId);
        $orders= new Orders();
        try{
            $orders= $orders->addOrder($request, $product, $this->getUser());
            $errors = $validator->validate($orders);
            if(count($errors)>0){
                $serializedUser = new Response([],(string) $errors,500);
                return $serializedUser->toJson();
            }
            //save order
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($orders);
            $entityManager->flush();

            $serialized= new Response([], "order saved successfully" ,200);
            return $serialized->toJson();
        }
        catch(DBALException $e){
            $errorMessage = $e->getMessage();

            $serialized = new Response([],$errorMessage,500);
            return $serialized->toJson();
        }
        catch(\Exception $e){
            $errorMessage = $e->getMessage();

            $serialized = new Response([],$errorMessage,500);
            return $serialized->toJson();
        }
    }

    /**
     * Show an order
     * @Route("/api/orders/{id}", name="show_order", methods="GET")
     */
    public function showOrder($id, OrdersRepository $OrdersRepository){

        $order =  $OrdersRepository->getOrder($id);

        $serialized= new Response($order, "" ,200);
        return $serialized->toJson();
    }

    /**
     * Update an order
     * @Route("/api/orders/{id}/update", methods="POST")
     */
    public function update($id, Request $request){
        $todayTime = new \DateTime();
        $order =  $this->getDoctrine()
                            ->getRepository(Orders::class)
                            ->find($id);

        $productId = $request->request->get('product_id');

        $interval=$todayTime->diff($order->getShippingDate());

         //fetch product
        $product =  $this->getDoctrine()
                         ->getRepository(Products::class)
                         ->find($productId);

        //update orders
        $updateOrder= $order->updateOrder($request, $product, $this->getUser());

        //check interval before updating
        if($interval->format('%a') >= 1 ){
            //save order
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $serialized= new Response($updateOrder->getOrder(), "order updated successfully" ,200);
            return $serialized->toJson();
        }
        else{
            $serialized= new Response([], "You can not update the order, because shipping is started" ,400);
            return $serialized->toJson();
        }
    }

    /**
     * @Route("/movies/{id}", methods="DELETE")
     */
    public function delete($id){

    }
}
