<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @Assert\NotBlank(message="please insert order code")
     * @ORM\Column(type="integer")
     * @Assert\Positive(message="order code has to be integer")
     */
    private $orderCode;

    /**
     * @Assert\NotBlank(message="please insert product id")
     * @ORM\Column(type="integer")
     * */
    private $productId;

    /**
     * @Assert\NotBlank(message="please insert quantity")
     * @ORM\Column(type="integer")
     */
    private $quntity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="datetime")
     */
    private $shippingDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Products", inversedBy="orders", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * update and order
     */
    public function updateOrder(Request $request, $product, $user) : self {
        $orderCode = $request->request->get('order_code');
        $address = $request->request->get('address');
        $shippingDate = new \DateTime($request->request->get('shipping_date'));
        $quantity = $request->request->get('quantity');
        $productId = $request->request->get('product_id');
        //set orders
        $orders= $this;
        $orders->setQuntity($quantity);
        $orders->setOrderCode($orderCode);
        $orders->setAddress($address);
        $orders->setShippingDate($shippingDate);
        $orders->setUser($user);
        $orders->setProduct($product);

        return $orders;
    }

    /**
     * add an order
     */
    public function addOrder(Request $request, $product, $user) : self {
        $orderCode = $request->request->get('order_code');
        $address = $request->request->get('address');
        $shippingDate = new \DateTime($request->request->get('shipping_date'));
        $quantity = $request->request->get('quantity');
        $productId = $request->request->get('product_id');
        //set orders
        $orders= $this;
        $orders->setQuntity($quantity);
        $orders->setOrderCode($orderCode);
        $orders->setAddress($address);
        $orders->setShippingDate($shippingDate);
        $orders->setUser($user);
        $orders->setProduct($product);

        return $orders;
    }

    /**
     * get order structure
     */
    public function getOrder(): array {
        $order=[
            'id'=> $this->getId(),
            'user'=> $this->getUser()->getId(),
            'order_code'=> $this->getOrderCode(),
            'product_id'=> $this->getProductId(),
            'shipping_date'=> $this->getShippingDate()->format('Y-m-d'),
            'address'=> $this->getAddress(),
            'quantity'=> $this->getQuntity(),
        ];

        return $order;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getOrderCode(): ?int
    {
        return $this->orderCode;
    }

    public function setOrderCode(int $orderCode): self
    {
        $this->orderCode = $orderCode;

        return $this;
    }

    public function getProductId(): ?int
    {
        return $this->productId;
    }

    public function setProductId(int $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getQuntity(): ?int
    {
        return $this->quntity;
    }

    public function setQuntity(int $quntity): self
    {
        $this->quntity = $quntity;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getShippingDate(): ?\DateTimeInterface
    {
        return $this->shippingDate;
    }

    public function setShippingDate(\DateTimeInterface $shippingDate): self
    {
        $this->shippingDate = $shippingDate;

        return $this;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(Products $product): self
    {
        $this->product = $product;

        return $this;
    }
}
