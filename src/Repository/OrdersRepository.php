<?php

namespace App\Repository;

use App\Entity\Orders;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Orders|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orders|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orders[]    findAll()
 * @method Orders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Orders::class);
    }

    // /**
    //  * @return Orders[] Returns an array of Orders objects
    //  */

    public function transform(Orders $orders)
    {
        $order=[
            'id'=> $orders->getId(),
            'user'=> $orders->getUser()->getId(),
            'order_code'=> $orders->getOrderCode(),
            'product_id'=> $orders->getProductId(),
            'shipping_date'=> $orders->getShippingDate()->format('Y-m-d'),
            'address'=> $orders->getAddress(),
            'quantity'=> $orders->getQuntity(),
        ];

        return $order;
    }

    public function getOrder($id){
        $order = $this->find($id);
        $transformed= $this->transform($order);
        return $transformed;
    }

    public function getAllOrders($user){
        $orders= $this->findByUser($user);
        $ordersArray=[];

        foreach($orders as $item){
            $ordersArray= $this->transform($item);
        }

        return $ordersArray;
    }
}
