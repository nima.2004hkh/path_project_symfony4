<?php
namespace App\Utils;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class Response{
    private $data, $mesaage, $status;
        function __construct($data, $message, $status){
            $this->data= $data;
            $this->message= $message;
            $this->status= $status;
        }

    public function toJson(){
        $data=[
            "data"=>$this->data,
            "message"=> $this->message,
            "status" => $this->status
        ];

        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object;
            },
        ];
       
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        $encoder = new JsonEncoder();

        $serialized=  new Serializer([$normalizer],[$encoder]);

        return JsonResponse::fromJsonString($serialized->serialize($data,'json', ['groups' => ['normal']]));
    }
}
